# Post-scarcity Map

The project is currently live at: https://postscarcitymap.org/

Discord: https://discord.com/invite/vhc8EZkmEv

Launch Post: https://www.reddit.com/r/singularity/comments/xzwt0c/introducing_the_basic_postscarcity_map/

[TOC]

# Purpose

A society in which all basic human needs are provided at zero or very low cost without significant human work can be defined as **basic post-scarcity**. 

Our civilization is getting close to the point where this is technically feasible. 

To reach this milestone as soon as possible, we introduce here the Post-scarcity Map project, an effort to map the current technological state of the art and to understand how far we are from a basic post-scarcity society. 

This project is an attempt to provide unbiased answers to the following questions:

- What technological advancements are needed to reach basic post-scarcity?

- What is the state of the art, what resources are available to learn about it and who is currently working on improving it?

- How far are we from achieving post-scarcity and what are the bottlenecks?

We believe that having a searchable and living assessment of the state of the art will enable people who want to work towards this goal to know what is needed, what is currently feasible and who is currently working on what.

# Approach

To accomplish this, the basic idea is to build two maps, each with a specific purpose.

1. Deconstruct the basic needs needed to reach basic post-scarcity and the technical milestones needed to satisfy them with minimal human work at the minimum cost

2. Show the state of the art for each of the technical milestones and their corresponding fields

We are aware of the many limitations of this approach and in particular we know that technical bottlenecks are not the only roadblocks to a basic post-scarcity civilization. 

However, we also think that it is not unreasonable to assume that reducing the cost and the manpower associated with fulfilling basic human needs will make it easier for public and / or private actors to provide them as widely as possible. 

# Maps

This repo contains the two maps mentioned above. 

- [**Needs**](https://postscarcitymap.org/needs.html) goes from general need areas to specific need milestones.
    
    - Each basic need is broken down in milestones to be tracked.  

    - Each need milestone includes a quantitative measure derived from one or several technical milestones to mark it as completed (100%).

    - Current state of the art in the associated technical milestones determines the **completion %**.

- [**Tech**](https://postscarcitymap.org/tech.html) goes from general fields to specific technical milestones.

    - Technical milestones are tracked alongside their sources.

    - Milestones are sorted in three levels according to their reliability: **claimed** (one party), **verified** (independent verification), and **public** (multiple independent verifications).

    - References to the current technological state of the art and associated learning resources are collected in separate pages forming a [**library**](https://gitlab.com/postscarcity/library). 


# Contributing

The project is completely open source, community-driven and non-profit. We are actively looking for contributors.

Involvement should be strictly non-commercial and from individuals, to keep the project unbiased. 
The cooperative nature of the work is key to minimize individuals’ conflict of interest.

At this stage we need help creating technical milestones. 

Domain experts are particularly welcome to shine light on the state of the art for the relevant milestones in their respective fields.

Additionally, anyone with reference materials and / or knowledge of people currently involved in solving these problems can directly contribute to the [library](https://gitlab.com/postscarcity/library/-/blob/main/CONTRIBUTING.md).

If you want to help in any capacity, please read [here](https://gitlab.com/postscarcity/map/-/blob/main/CONTRIBUTING.md).
