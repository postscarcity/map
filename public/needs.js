// Tree

// Tidy tree layout from https://observablehq.com/@d3/tree
// Based on code from https://codesandbox.io/s/d3-tidy-tree-example-forked-l69o1?file=/src/index.js

async function getData(file) {
  const response = await fetch(file);
  return response.json();
}

const data = await getData("./needs.json");

const margin = { top: 0, right: 50, bottom: 100, left: 80 };
const height = 1000;
const width = height * 2.5;
const root = d3.hierarchy(data);
const dx = height / 25;
const dy = height / 3;
const tree = d3.tree().nodeSize([dx, dy]);
const diagonal = d3
  .linkHorizontal()
  .x((d) => d.y)
  .y((d) => d.x);

root.x0 = dy / 2;
root.y0 = 0;
root.descendants().forEach((d, i) => {
  d.id = i;
  d._children = d.children;
});

tree(root);

var svg = d3.select(".tree").append("svg")
.attr("width", 2000)
.attr("height", 1200);

const gLink = svg
  .append("g")
  .attr("fill", "none")
  .attr("stroke", "#999")
  .attr("stroke-opacity", 0.4)
  .attr("stroke-width", 1);

const gNode = svg
  .append("g")
  .attr("cursor", "pointer")
  .attr("pointer-events", "all");

svg.call(d3.zoom()
    .extent([[0, 0], [width, height]])
    .scaleExtent([0.5, 3])
    .on("zoom", zoomed));

function zoomed({transform}) {
  gLink.attr("transform", transform);
  gNode.attr("transform", transform);
}

// Toggle all children of a node
function toggleAll(d) {
  if (d.children) {
    d.children.forEach(toggleAll);
    toggle(d);
  }
}

// Toggle children
function toggle(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
}

// Linebreak for longer node names
function lineBreak(text, width) {
  text.each(function () {
    var el = d3.select(this);
    let words = el.text().split(' ');
    let wordsFormatted = [];

    let string = '';
    for (let i = 0; i < words.length; i++) {
      if (words[i].length + string.length <= width) {
        string = string + words[i] + ' ';
      }
      else {
        wordsFormatted.push(string);
        string = words[i] + ' ';
      }
    }
    wordsFormatted.push(string);

    el.text('');
    for (var i = 0; i < wordsFormatted.length; i++) {
      var tspan = el.append('tspan').text(wordsFormatted[i]);
      if (i > 0 && i < 2)
        tspan.attr('x', -width / 1.5).attr('y', width / 1.4);
    }
  });
}

function percentageToHsl(percentage, saturation, lightness) {
  return 'hsl(' + percentage + ', ' + saturation + ',' + lightness + ')';
}

// Initialize display to open tree
update(root);

function update(source) {
  const duration = d3.event && d3.event.altKey ? 2500 : 250;
  const nodes = root.descendants().reverse();
  const links = root.links();

  // Compute the new tree layout

  let left = root;
  let right = root;
  root.eachBefore((node) => {
    if (node.x < left.x) left = node;
    if (node.x > right.x) right = node;
  });

  const height = right.x - left.x + margin.top + margin.bottom;

  const transition = svg
    .transition()
    .duration(duration)
    .attr("viewBox", [-margin.left, left.x - margin.top, width, height])
    .tween(
      "resize",
      window.ResizeObserver ? null : () => () => svg.dispatch("toggle")
    );

  // Update the nodes
  const node = gNode.selectAll("g")
    .data(nodes, (d) => d.id);

  // Enter any new nodes at the parent's previous position
  const nodeEnter = node
    .enter()
    .append("g")
    .attr("transform", (d) => `translate(${source.y0},${source.x0})`)
    .attr("fill-opacity", 0)
    .attr("stroke-opacity", 0)
    .on("click", (event, d) => {
      d.children = d.children ? null : d._children;
      update(d);
    });

  nodeEnter
    .append("circle")
    .attr("r", 12)
    .attr("fill", (d) => (d._children ? percentageToHsl(d.data.completion, '65%', '75%') : percentageToHsl(d.data.completion, '65%', '50%')))
    .attr("stroke-width", 10);

  nodeEnter
    .append("text")
      .text((d) => d.data.id)
      .call(lineBreak, 30)
      .attr("dy", "0.32em")
      .attr("x", -20)
      .attr("text-anchor", "end")
      .attr("font-weight", (d) => (d._children ? "" : "bold"))
    .clone(true)
    .lower()
    .attr("stroke-linejoin", "round")
    .attr("stroke-width", 10)
    .attr("stroke", "white");
  
  nodeEnter
    .append("text")
      .text((d) => d.data.completion)
      .attr("dy", "0.31em")
      .attr("text-anchor", "middle")
      .style("font", "12px sans-serif")
      .style("fill", "white");

  nodeEnter
    .append("title")
      .text((d) => d.data.condition);

  // Transition nodes to their new position
  node
    .merge(nodeEnter)
    .transition(transition)
    .attr("transform", (d) => `translate(${d.y},${d.x})`)
    .attr("fill-opacity", 1)
    .attr("stroke-opacity", 1);

  // Transition exiting nodes to the parent's new position
  node
    .exit()
    .transition(transition)
    .remove()
    .attr("transform", (d) => `translate(${source.y},${source.x})`)
    .attr("fill-opacity", 0)
    .attr("stroke-opacity", 0);

  // Update the links
  const link = gLink.selectAll("path").data(links, (d) => d.target.id);

  // Enter any new links at the parent's previous position
  const linkEnter = link
    .enter()
    .append("path")
    .attr("d", (d) => {
      const o = { x: source.x0, y: source.y0 };
      return diagonal({ source: o, target: o });
    });

  // Transition links to their new position
  link.merge(linkEnter).transition(transition).attr("d", diagonal);

  // Transition exiting nodes to the parent's new position
  link
    .exit()
    .transition(transition)
    .remove()
    .attr("d", (d) => {
      const o = { x: source.x, y: source.y };
      return diagonal({ source: o, target: o });
    });

  // Stash the old positions for transition
  root.eachBefore((d) => {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}
