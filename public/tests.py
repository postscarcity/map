import networkx as nx


def check_nodes_duplication(G, node_list):

    for node in node_list:
        if len(list(G.predecessors(node))) > 1:
            #raise NameError("A node appears multiple times:", node)
            print("A node appears multiple times:", node)
            