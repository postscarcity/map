# To generate the JSON: python3 tech.py
import networkx as nx
from networkx.readwrite import json_graph
import json
from tests import check_nodes_duplication
from tree_utils import default_values, compute_completion, create_lib_urls

G = nx.DiGraph()

def add(parent, child, **attributes):
    G.add_node(child, **attributes)
    G.add_edge(parent, child)

## === TREE ===

root = 'Tech'

# Level 0

add(root, 'Energy')
add(root, 'Matter')
add(root, 'Information')

# Level 1

add('Energy', 'Energy generation')
add('Energy', 'Energy storage')
add('Energy', 'Energy distribution')

add('Matter', 'Resources')
add('Matter', 'Logistics')
add('Matter', 'Production')

add('Information', 'Data acquisition')
add('Information', 'Data storage')
add('Information', 'Data retrieval')
add('Information', 'Data elaboration')
add('Information', 'Data transmission')

# Level 2

add('Energy generation', 'Petroleum')
add('Energy generation', 'Coal')
add('Energy generation', 'Natural gas')
add('Energy generation', 'Nuclear')
add('Energy generation', 'Hydroelectric')
add('Energy generation', 'Wind')
add('Energy generation', 'Solar')

add('Energy storage', 'Fossil Fuel Energy Storage')
add('Energy storage', 'Mechanical energy storage')
add('Energy storage', 'Electrical energy storage')
add('Energy storage', 'Biological energy storage')
add('Energy storage', 'Electrochemical energy storage')
add('Energy storage', 'Thermal energy storage')
add('Energy storage', 'Chemical energy storage')

add('Energy distribution', 'Electric power distribution')

add('Resources', 'Organic')
add('Resources', 'Inorganic')

add('Logistics', 'Production logistics')
add('Logistics', 'Maintenance logistics')
add('Logistics', 'Repair logistics')
add('Logistics', 'Disposal logistics')
add('Logistics', 'Transport logistics')

add('Production', 'Food production')
# ...
add('Production', 'Robotic arm')

add('Data acquisition', 'Sensors')
# ...

add('Data elaboration', 'Visual data elaboration')


# Level 3

add('Transport logistics', 'Air Transport')
add('Transport logistics', 'Land Transport')
add('Transport logistics', 'Sea Transport')
add('Transport logistics', 'Space Transport')

# https://www.variohm.com/news-media/technical-blog-archive/types-of-sensors
add('Sensors', 'Position sensors', condition='Sensor measuring less than X capable of recording position with less than Y accuracy with maximum Z power consumption')
add('Sensors', 'Pressure sensors')
add('Sensors', 'Temperature sensors')
add('Sensors', 'Force sensors')
add('Sensors', 'Vibration sensors')
add('Sensors', 'Piezo sensors')
add('Sensors', 'Fluid property sensors')
add('Sensors', 'Humidity sensors')
add('Sensors', 'Strain sensors')
add('Sensors', 'Photo optic sensors')

add('Food production', 'Meal preparation')

add('Robotic arm', 'Collaborative robotic arm')

add('Visual data elaboration', 'Machine vision')

# Level 4

add('Land Transport', 'Rail Transport')
add('Land Transport', 'Road Transport')

add('Meal preparation', 'Meal preparation robots')

add('Collaborative robotic arm', 'Robot actuators')
add('Collaborative robotic arm', 'Robot end effectors')

add('Machine vision', 'RGB machine vision')
add('Machine vision', 'RGBD machine vision')


# Level 5

add('Rail Transport', 'Autonomous Subway Trains', condition="Driverless Subway Trains able to operate on full schedule (18h to 24h per day)", completion = 1.0)
add('Rail Transport', 'Autonomous Intercity Trains')

add('Road Transport', 'Freight Transport')

mpr = 'Meal preparation robots'
add(mpr, mpr + ' intervention time', condition="Food Robot with average time between minor human interventions = 3 hours", completion = 0)
add(mpr, mpr + ' number of ingredients', condition="Food Robot can handle a large varieties of ingredients: 150", completion = 0, status="Verified")
add(mpr, mpr + ' salad', condition="Food Robot can prepare a salad from fresh ingredients, with less than 1 mistake every 200 salads", completion = 0)
add(mpr, mpr + ' pizza', condition="Food Robot can prepare a pizza from fresh ingredients, with less than 1 mistake every 200 pizzas", completion = 0)
add(mpr, mpr + ' burger', condition="Food Robot can prepare a burger from fresh ingredients, with less than 1 mistake every 200 burgers", completion = 0)
add(mpr, mpr + ' rice', condition="Food Robot can prepare a rice bowl from fresh ingredients, with less than 1 mistake every 200 bowls", completion = 0)
add(mpr, mpr + ' dynamic env', condition="Food robot can deployed in dynamic kitchen environment, with less than 1 mistake every 2 hours", completion = 0)
add(mpr, mpr + ' recipes', condition="Food Robot system able to cook a large number of recipes", completion = 0, boolean="True", simultaneous_requirements=[mpr + ' salad',mpr + ' pizza',mpr + ' burger',mpr + ' rice'])
add(mpr, mpr + ' learn from demonstration', condition="Food robot can learn recipe in dynamic kitchen environment from less than 100 human demonstrations", completion = 0)
add(mpr, mpr + ' throughput', condition="Food Robot has a throughput not lower than 20% of an in person human staff", completion = 0)

add('Robot end effectors', 'Suction grippers')
add('Robot end effectors', 'Robot hands')
add('Robot end effectors', 'Food end effectors')

add('RGB machine vision', 'Object detection')
add('RGB machine vision', 'Object segmentation')

# Level 6
add('Freight Transport', 'Autonomous Freight Transport', condition="Level 5 Autonomous Freight Transport", completion = 0)
add('Freight Transport', 'Autonomous Freight on highways', condition="Autonomous Truck able to travel on US-style highways for 500 km without human intervention", completion = 0.258)

fee = 'Food end effectors'
add(fee, fee + ' delicate', condition="Robot end effectors can handle delicate food, with less than 1 mistake every 200 ingredients", completion = 0)
add(fee, fee + ' affordable', condition="Food end effectors are available for less than 500 USD", completion = 0)
add(fee, fee + ' mature', condition="Affordable Food end effectors for delicate food", completion = 0, boolean="True", simultaneous_requirements=[fee + ' delicate',  fee + ' affordable'])

add('Object detection', 'Food detection dish', condition="Food machine vision detection algorithms can recognize dishes with 99% accuracy", completion = 0)
add('Object detection', 'Food detection ingredients', condition="Food machine vision detection algorithms can recognize individual ingredients 99% accuracy", completion = 0)

# === END ===

node_list = list(G.nodes())
reversed_node_list = node_list[::-1]

# Assign default values
default_values(G, reversed_node_list, {"completion":0.0, "status":"Claimed"})

# Iterate over nodes and compute the completion percentage
compute_completion(G, reversed_node_list, verbose=False)
    
create_lib_urls(G, node_list)

# Sanity Checks
check_nodes_duplication(G, node_list)

# JSON data to export
data = json_graph.tree_data(G, root=root)

# Dump in local JSON file
with open('tech.json', 'w') as f:
    json.dump(data, f)
