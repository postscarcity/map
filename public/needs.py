# To generate the JSON: python3 needs.py
import networkx as nx
from networkx.readwrite import json_graph
import json
import tech
from tests import check_nodes_duplication
from tree_utils import default_values, compute_completion, need_completion

G = nx.DiGraph()

def add(parent, child, **attributes):
    G.add_node(child, **attributes)
    G.add_edge(parent, child)

## === PARAMS ===

tt = tech.G.nodes # Tech Tree

# Need tiers
tier0 = "Basic"  # Default value if unspecified.
tier1 = "Important" 

## === TREE ===

root = 'Needs'

## Level 0

# add(root, 'Oxygen')

add(root, 'Water')
add(root, 'Food')
add(root, 'Clothing', completion=0.9) # TODO attach to tech milestones
add(root, 'Housing')
add(root, 'Healthcare')
add(root, 'Communication')
add(root, 'Education')
add(root, 'Transportation')

# Level 1

add('Water', 'Drinking water')
add('Water', 'Domestic water')

add('Food', 'Ingredients')

mpr = 'Meal preparation robots'
add('Food', 'Ready meals', completion=need_completion(tt, 
[mpr + ' intervention time', mpr + ' number of ingredients', mpr + ' salad', mpr + ' pizza', mpr + ' burger', mpr + ' rice', mpr + ' dynamic env', mpr + ' recipes', mpr + ' learn from demonstration', mpr + ' throughput', 'Food detection dish', 'Food detection ingredients', 'Food end effectors mature']))

add('Transportation', 'Urban')
add('Transportation', 'Freight Transport', completion=need_completion(tt,['Autonomous Freight Transport', 'Autonomous Freight on highways']))

# Level 2


# Level 3



# Level 4



# Level 5



# === END ===

node_list = list(G.nodes())
reversed_node_list = node_list[::-1]

# Assign default values
default_values(G, reversed_node_list, {"completion":0.0, "need_tier":"Basic"})

# Iterate over nodes and compute the completion percentage
compute_completion(G, reversed_node_list, verbose=True)

# Sanity Checks
check_nodes_duplication(G, node_list)

# JSON data to export
data = json_graph.tree_data(G, root=root)

# Dump in local JSON file
with open('needs.json', 'w') as f:
    json.dump(data, f)