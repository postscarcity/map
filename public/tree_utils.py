
def default_values(G, node_list, defaults):
    '''
    Assign default values to the tree.

        Parameters:
                G: networkx DiGraph
                node_list: List of nodes
                defaults (dict): 
    '''
    for node in node_list:
        for k, v in defaults.items():
            if not (k in G.nodes[node]):
                G.nodes[node][k] = v


def compute_completion(G, node_list, verbose=False):
    '''
    Iterate over nodes and compute the completion percentage.

        Parameters:
                G: networkx DiGraph
                node_list: List of nodes
    '''
    for node in node_list:

        completion_percentage = 0

        # Iterate over node successors
        n_successors = len(list(G.successors(node)))
        if n_successors > 0:
            successors = G.successors(node)
            
            for suc in successors:
                completion_percentage += G.nodes[suc]['completion']
            completion_percentage /= n_successors
            
            G.nodes[node]['completion']  = round(completion_percentage, 2)

        if verbose:
            print("*  ", node, G.nodes[node])

    for node in node_list:
        G.nodes[node]['completion'] = int(100 * G.nodes[node]['completion'])


def need_completion(tech_tree_node_list, tech_dependencies, weights=None, req=None):
    '''
    Compute the need completion from a weighted sum of tech_dependencies.

        Parameters:
                tech_tree_node_list: List of nodes of the tech tree
                tech_dependencies : List of strings, with the tech node names
                weights: List of floats with the weights of every tech. They must sum to 1.
                req: List of floats with the tech completion amount which is considered enough for need completion. The must be 0 < x <= 1.
    '''
    lt = len(tech_dependencies)
    assert(lt > 0)

    if weights:
        assert(sum(weights) == 1)
    else: # default is uniform weights.
        weights = [1.0/lt] * lt 

    if req:
        assert(lt == len(req))
    else: # default is full tech development.
        req = [100] * lt 

    completion = sum([tech_tree_node_list[t]['completion'] / r * w for (t,r,w) in zip(tech_dependencies, req, weights)])
    return completion
    

def create_lib_urls(G, node_list):
    '''
    Assign library urls to every node.
    '''

    for node in node_list:
        
        n_successors = len(list(G.successors(node)))
        if n_successors == 0:
            milestone = (node.translate(str.maketrans({' ': '_', '-': '_'}))).lower()
            G.nodes[node]['url'] = "https://gitlab.com/postscarcity/library/-/blob/main/" + milestone + ".md"
