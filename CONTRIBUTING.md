# Contributing

> If you are interested in sharing your material on any field covered in the [tech map](https://postscarcitymap.org/tech.html), please visit the [library](https://gitlab.com/postscarcity/library/-/tree/main/). 

**Summary**: Be as unambiguous as possible and reference any figure you provide. If you want to contribute on a regular basis or participate in the discussion join us on [Discord](https://discord.gg/vhc8EZkmEv).

[TOC]

# Areas

Thank you for taking the time to contribute and to read our guidelines.

We looking for help in any of the following steps:

1. Create need milestones in [`needs.py`](https://gitlab.com/postscarcity/map/-/blob/main/public/needs.py)

2. Add tech milestones dependencies to need milestones

3. Add tech milestones in [`tech.py`](https://gitlab.com/postscarcity/map/-/blob/main/public/tech.py)

4. Find state of the art for tech milestones

5. Update need milestone completion %

6. Add links to relevant material and people in the [`library`](https://gitlab.com/postscarcity/library/-/tree/main/)

  
# Guidelines

We aim for the contributions to the maps to be: 

**Factual**

Every figure needs to be referenced with accessible and reliable sources. 

**Transparent**

Disclose conflicts of interest or other biases you may be aware of. 

**Neutral**

Report figures and material with the minimum amount of editorial editing possible. 

**Global** 

All needs and tech milestones refer to the highest global standard.

**Necessary**
Insert only tech needed to fulfill the basic needs listed, nothing more.

# Tech tree

## Milestone types

- **Atomic** milestones, for which it's possible to objectively and measurably quantify the state of advancement by looking at one performance number. Make atomic milestones as **granular** as possible. 

   `Boolean` (0 or 1) or `continuos` completion value (0 to 100) are both accepted. 

- **Aggregate** milestones, which aggregate multiple atomic milestones. 

   Aggregate milestones require all the linked Atomic milestones to be true at the same time in the same system in order to be considered completed. 

   `Boolean` (0 or 1) completion value.

## Measurability

To specify the completion percentage use the measurable attribute cited in the milestone description and assign a linear score based on the state of the art. 

> **Example**: a self-driving car milestone may be defined completed (100%) when a car is able to self-drive for an average of 100 Km in a given city without human intervention. If the state of the art is 60 Km, assign a score of 60% (which is 0.6 as value between 0 and 1).

## Reliability

Specify the reliability level of the tech milestone completion. There are 3 main levels: 

- **Claimed**

Performance data has only been published by the claiming party.

> **Examples**: preprint papers, company press releases.

- **Verified**

Performance data has been scrutinised / verified by at least one external third party not affiliated with the original claiming party.

> **Examples**: published paper, product beta testing.

- **Public**

Product / service is available and can be utilised as intented.

> **Examples**: product available on the market. 

## Limitations

We are aware of the limitations of this approach (among others, non-linearity of progress) but showing current state of the art and providing quantifiable milestones are still useful on their own.

# Needs tree

Unfortunately needs are even less objective, so a consensus must be reached. 

1. Keep it simple, we are looking at basic needs. The key idea is to prioritise needs that would maximise the increase in [quality adjusted life years](https://en.wikipedia.org/wiki/Quality-adjusted_life_year).

2. Any need mileston must list all and only the tech milestone dependencies needed for it to be completed.

3. Any root need added to the need tree requires opening an issue to discuss the motivation for the addition and plan on how to maintain it / expand it.

# Issues

## Bugs

- Create `Bug` issues for bugs as soon as you see them. Usual rules apply: explain the problem, desired behaviour and steps to reproduce the error.

## Enhancements

- Create `Enhancement` issues for any major changes and enhancements that you wish to make. Motivate your request and get community feedback to decide whether to proceed with it or not.

   If you are willing to work on it yourself, join us on [Discord](https://discord.gg/vhc8EZkmEv) to coordinate our work and get full access.

## Feature requests

* - Create `Feature Request` issues for any major changes and enhancements that you wish to make. Motivate your request and get community feedback to decide whether to proceed with it or not.

   If you are willing to work on it yourself, join us on [Discord](https://discord.gg/vhc8EZkmEv) to coordinate our work and get full access.


# Review process

Generally speaking, the review process for the issues above is the following:

1. Report issues as outlined above.

2. For small contributions you can expect one of the project maintainers to take care of it once acknowledged.

3. For larger contributions or new important features the maintainers may use [Discord](https://discord.gg/vhc8EZkmEv) to gather feedback from the community.

# Formatting

Please follow existing formatting conventions.

In particular, follow the existing spacing conventions when inserting need / tree milestones.
